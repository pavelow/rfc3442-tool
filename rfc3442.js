//This function gets the form data and creates a value string
function getForm() {
  document.getElementById("value").value = routes("value");
}

function copyURL() {
  var copyText = document.getElementById("value");
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/
  document.execCommand("copy");
}

function validateIP(addr) {
  if(addr.match(/^([0-9]{1,3}\.){3}[0-9]{1,3}$/)) { //Correct form, test range
    numbers = addr.split(".").map(x => parseInt(x));
    for (n in numbers) {
      if (numbers[n] > 255 | numbers[n] < 0) {
        return 0;
      }
    }
    return 1;
  }
  return 0;
}

function validateCIDR(addr) {
  if (addr.match(/^([0-9]{1,3}\.){3}[0-9]{1,3}\/[0-9]{1,2}$/)) { //Correct form, test range
    a = addr.split("/");
    if(validateIP(a[0])) {
      if(parseInt(a[1]) <= 32 & parseInt(a[1]) >= 0) {
        return 1;
      }
    }
  }
  return 0;
}

//Assume validated before this is called
function cidrToString(string) {
  str = "";
  a = string.split("/");
  address = a[0].split('.').map(x => parseInt(x));
  netmask = parseInt(a[1]);

  str += ("00" + netmask.toString(16)).slice(-2);

  octets = Math.floor(netmask / 8);
  for(var i = 0; i < octets; i++) {
    str += ("00" + address[i].toString(16)).slice(-2)
  }

  lastOctet = netmask%8;
  if(lastOctet != 0) {
    mask = 0x00;
    for (var i = 0; i < lastOctet; i++) {
      mask |= 0x80 >> i;
    }

    str += ("00" + (address[octets] & mask).toString(16)).slice(-2)
  }
  return str;
}

function dec2bin(dec){
    return (dec >>> 0).toString(2);
}

function ipToString(string) {
  str = "";
  addr = string.split(".").map(x => parseInt(x));
  for(var i = 0; i < 4; i++) {
    str += ("00" + addr[i].toString(16)).slice(-2);
  }
  return str;
}

function routes(action) {
  if( typeof routes.n == 'undefined' ) {
          routes.n = 0;
  }

  switch(action) {
    case "add":
      var html = document.createElement("fieldset");
      html.classList = ["route"];
      html.id = "r" + routes.n;
      html.innerHTML += "<button style=\"height: 2em;\" onclick=\"routes('del')\">x</button>";
      html.innerHTML += "<label >Subnet </label>";
      html.innerHTML += "<input class=\"network\" type=\"text\" id=\"nw" + routes.n + "\" value=\"172.16.0.0/24\" placeholder=\"192.168.0.0/24\">";
      html.innerHTML += "<label > via </label>";
      html.innerHTML += "<input class=\"gateway\" type=\"text\" id=\"gw" + routes.n + "\" value=\"10.1.101.1\" placeholder=\"192.168.1.1\">";


      document.getElementById("reminders").append(html);
      routes.n++;
      getForm();
      break;

    case "del":
      document.activeElement.parentElement.parentElement.removeChild(document.activeElement.parentElement);
      getForm();
      break;

    case "value":
      str = "0x";
      for(var i = 0; i < routes.n; i++) {
        if(document.getElementById("r" + i) != null) {
          network = document.getElementById("nw" + i).value;
          gateway = document.getElementById("gw" + i).value;

          if(validateIP(gateway) & validateCIDR(network)) {

          } else {
            str = "error";
          }

          if(str != "error") {
            str += cidrToString(network);
            str += ipToString(gateway);
          }
        }
      }
      return str;
  }
}
